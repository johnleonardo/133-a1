package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.events.ActionEvent;

public class CommandRightHeading extends Command {
	
	// the game world referring to
	private GameWorld gw;
	
    /**
     * Constructor for right heading command
     * @param gw
     */
    public CommandRightHeading(GameWorld gw) {
    	
    	// pass to parent
        super("Right");
        
        // set gw
        this.gw = gw;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent ev) {
    	
    	// change heading right 5 degrees
        gw.changeHeading(5);
    }
}