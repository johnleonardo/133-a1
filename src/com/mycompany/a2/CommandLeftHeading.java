package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.events.ActionEvent;

public class CommandLeftHeading extends Command {
	
	// the game world referring to
	private GameWorld gw;
	
    /**
     * Constructor for left heading command
     * @param gw
     */
    public CommandLeftHeading(GameWorld gw) {
    	
    	// pass to parent
        super("Left");
        
        // set gw
        this.gw = gw;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent ev) {
    	
    	// change heading left 5 degrees
        gw.changeHeading(-5);
    }
}
