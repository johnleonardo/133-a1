package com.mycompany.a2;

public abstract class Movable extends GameObject {
	
	// required attributes
	private int heading;
	private int speed;
	
	/**
	 * @param size
	 * @param x
	 * @param y
	 */
	public Movable(int size, double x, double y) {
		
		// pass to parent's constructor
		super(size, x, y);
	}
	
	@Override
	public String toString() {
		
		// build strings
		String headingDesc = "heading: " + heading + "°";
		
		String speedDesc = "speed: " + speed;
		
		// return result string
		return super.toString() + ", " 
								+ headingDesc + ", "
								+ speedDesc;
	}
	
	public void move() {
		
		// calculate angle andchange of x and y
		int angle = 90 - this.getHeading();
		double deltaX = Math.cos(Math.toRadians(angle));
		double deltaY = Math.sin(Math.toRadians(angle));
		
		// set new x and y
		this.setX((float) deltaX + (float) getX());
		this.setY((float) deltaY + (float) getY());
	}
	
	/**
	 * getter for heading attribute
	 * @return the heading
	 */
	public int getHeading() {
		return heading;
	}

	/**
	 * setter for heading attribute
	 * @param heading the heading to set
	 */
	public void setHeading(int heading) {
		this.heading = this.heading + heading;
	}

	/**
	 * getter for speed attribute
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * setter for speed attribute
	 * @param speed the speed to set
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
}
