package com.mycompany.a2;

import com.codename1.charts.models.Point;

public abstract class Fixed extends GameObject {
	
	/**
	 * Constructor
	 * @param size
	 * @param x
	 * @param y
	 */
	public Fixed(int size, double x, double y) {
		super(size, x, y);
	}
	
	
	// not allowed to set X, so override and do nothing
	@Override
	public void setX(double x) {
		
	}
	
	// not allowed to set Y, so override and do nothing
	@Override
	public void setY(double y) {
		
	}
}