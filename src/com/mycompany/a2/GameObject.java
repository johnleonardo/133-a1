package com.mycompany.a2;

// imports
import com.codename1.charts.models.Point;
import com.codename1.charts.util.ColorUtil;

import java.util.Random;

public abstract class GameObject {
	
	// required atttributes
	private int color;
	private double x = 0;
	private double y = 0;
	private int size;
	
	/**
	 * @param size
	 * @param x
	 * @param y
	 */
	public GameObject(int size, double x, double y) {
		this.size = size;
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @return
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * @return
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * @param y
	 */
	public void setY(double y) {
		this.y = y;
	}
	
	/**
	 * @param x
	 */
	public void setX(double x) {
		this.x = x;
	}
	
	/**
	 * @param color
	 */
	public void setColor(int color) {
		this.color = color;
	}
	
	/**
	 * @return
	 */
	public int getColor() {
		return this.color;
	}
	
	public String toString() {
		
		// build string
		String myDesc = "loc=" + Math.round(x*10.0)/10.0 + "," + Math.round(y*10.0)/10.0 + 
				"color=[" + ColorUtil.red(this.color) + "," + ColorUtil.green(this.color) + "," + ColorUtil.blue(this.color) + "]";
		
		// result
		return myDesc;
	}
}
