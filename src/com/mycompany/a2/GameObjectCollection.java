package com.mycompany.a2;
import java.util.Vector;

public class GameObjectCollection implements ICollection {
	
	// vector that will be filled with game objects
	private Vector<GameObject> theCollection;

	@Override
	public void add(GameObject newObject) {
		// add object to collection
		theCollection.add(newObject);
	}

	@Override
	public IIterator getIterator() {
		// return the iterator that belongs to this collection
		return new SpaceVectorIterator();
	}
	
	private class SpaceVectorIterator implements IIterator {
		
		// pointer
		private int currElementIndex;
		
		// constructor
		public SpaceVectorIterator() {
			currElementIndex = -1;
		}

		@Override
		public boolean hasNext() {
			if (theCollection.size() <= 0) return false;
			return (currElementIndex != theCollection.size() - 1);
		}

		@Override
		public Object getNext() {
			currElementIndex++;
			return (theCollection.elementAt(currElementIndex));
		}
		
	}

}
