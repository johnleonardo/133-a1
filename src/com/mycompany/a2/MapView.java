package com.mycompany.a2;

import java.util.Observable;
import java.util.Observer;

import com.codename1.charts.util.ColorUtil;
import com.codename1.ui.Container;
import com.codename1.ui.plaf.Border;

public class MapView extends Container implements Observer {
	
	// reference to game world
	private GameWorld gw;
	
	// constructor
	public MapView(Observable observable) {
		
		// set gw to casted observable
		this.gw = (GameWorld) observable;
		
		// add observer
		gw.addObserver(this);
		
		// set border
		this.getAllStyles().setBorder(Border.createLineBorder(4, ColorUtil.MAGENTA));
	}
	
	// default constructor
	public MapView() {}

	@Override
	public void update(Observable observable, Object data) {
		// output game world info to console
		gw.map();
	}

}
