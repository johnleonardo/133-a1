package com.mycompany.a2;

// imports
import java.util.Random;

import com.codename1.charts.util.ColorUtil;

public class EnergyStation extends Fixed {
	
	// required attributes
	private int capacity;
	
	/**
	 * Constructor
	 * @param size
	 * @param x
	 * @param y
	 */
	public EnergyStation(int size, double x, double y) {
		// call parent's constructor
		super(size, x, y);
		
		// set color
		super.setColor(ColorUtil.rgb(0,0, 255));
		
		// set capacity
		capacity = size;
	}
	
	/**
	 * @return
	 */
	public int getCapacity() {
		return capacity;
	}
	
	/**
	 * @param capacity
	 */
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	@Override
	public String toString() {
		String parentDesc = super.toString();
		String myDesc = " capacity=" + capacity;
		return "Energy Station:" + parentDesc + myDesc;
	
	}
}
