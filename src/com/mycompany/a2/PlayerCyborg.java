package com.mycompany.a2;

public class PlayerCyborg extends Cyborg {

	
	/**
	 * @param size
	 * @param heading
	 * @param x
	 * @param y
	 */
	public PlayerCyborg(int size, int heading, double x, double y) {
		
		// pass to parent's constructor
		super(size, heading, x, y);
	}
	
	@Override
	public String toString() {
		String parentDesc = super.toString();
		return "Player Cyborg:" + parentDesc;
	}
	
}
