package com.mycompany.a2;

import java.util.Observable;
import java.util.Observer;

import com.codename1.charts.util.ColorUtil;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;

public class ScoreView extends Container implements Observer {
	
	// this ScoreView's game world reference
	private GameWorld gw;
	
	// labels to set
	private Label livesLabel;
	private Label livesValue;
	private Label clockLabel;
	private Label clockValue;
	private Label lastBaseLabel;
	private Label lastBaseValue;
	private Label energyLevelLabel; 
	private Label energyLevelValue; 
	private Label damageLevelLabel;
	private Label damageLevelValue;
	private Label soundLabel;
	private Label soundValue;
	
	// constructor
	public ScoreView(Observable gw) {
		// set gw
		this.gw = ((GameWorld) gw);
		
		// add this to observer
		gw.addObserver(this);
		
		// set flow layout
		this.setLayout(new FlowLayout(Component.CENTER));
		
		// set border
		this.getAllStyles().setBorder(Border.createLineBorder(4, ColorUtil.MAGENTA));
		
		// configure labels
		this.configureLabels();
		
		// add labels
		this.add(livesLabel);
		this.add(livesValue);
		this.add(clockLabel);
		this.add(clockValue);
		this.add(lastBaseLabel);
		this.add(lastBaseValue);
		this.add(energyLevelLabel);
		this.add(energyLevelValue);
		this.add(damageLevelLabel);
		this.add(damageLevelValue);
		this.add(soundLabel);
		this.add(soundValue);
		
	}
	
public void configureLabels() {
		
		// show lives
		livesLabel = new Label("Lives: ");
		livesValue = new Label("" + gw.getLives());
		livesLabel.getAllStyles().setFgColor(ColorUtil.BLUE);
		livesValue.getAllStyles().setFgColor(ColorUtil.BLUE);

		// show clock
		clockLabel = new Label("Clock: ");
		clockValue = new Label("" + gw.getClock());
		clockLabel.getAllStyles().setFgColor(ColorUtil.BLUE);

		// show last base
		lastBaseLabel = new Label("Last Base Reached: ");
		lastBaseValue = new Label("" + gw);
		lastBaseLabel.getAllStyles().setFgColor(ColorUtil.BLUE);
		lastBaseValue.getAllStyles().setFgColor(ColorUtil.BLUE);

		// show energy
		energyLevelLabel = new Label("Player Energy Level: ");
		energyLevelValue = new Label("" + gw.getEnergyLevel());
		energyLevelLabel.getAllStyles().setFgColor(ColorUtil.BLUE);
		energyLevelValue.getAllStyles().setFgColor(ColorUtil.BLUE);

		
		// show damage
		damageLevelLabel = new Label("Damage Level: ");
		damageLevelValue = new Label("" + gw.getDamageLevel());
		damageLevelLabel.getAllStyles().setFgColor(ColorUtil.BLUE);
		damageLevelValue.getAllStyles().setFgColor(ColorUtil.BLUE);
		
		// show sound
		soundLabel = new Label("Sound: ");
		soundValue = new Label("" + gw.isSound());
		soundLabel.getAllStyles().setFgColor(ColorUtil.BLUE);
		soundValue.getAllStyles().setFgColor(ColorUtil.BLUE);

		
	}

	@Override
	public void update(Observable observable, Object data) {
		
		// update everything
		livesValue.setText("" + gw.getLives());
		livesValue.getParent().revalidate();
		clockValue.setText("" + gw.getClock());
		clockValue.getParent().revalidate();
		lastBaseValue.setText("" + gw.getLastBaseReached());
		lastBaseValue.getParent().revalidate();
		energyLevelValue.setText("" + gw.getEnergyLevel());
		energyLevelLabel.getParent().revalidate();
		damageLevelValue.setText("" + gw.getDamageLevel());
		damageLevelValue.getParent().revalidate();
		soundValue.setText("" + gw.isSound());
		soundValue.getParent().revalidate();
	}
}
