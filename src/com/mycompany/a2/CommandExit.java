package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;

public class CommandExit extends Command {
	
	// game world
	private GameWorld gw;
	
	/**
	 * @param gw
	 */
	public CommandExit(GameWorld gw) {
		// call parent
		super("Exit");
		this.gw = gw;
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		
		// yes and no commands
		Command yes = new Command("yes");
		Command no  = new Command("no");
		
		// empty label
		Label label1 = new Label("");
		
		// confirmation
		Command c = Dialog.show("Are you sure you would like to exit?", label1, yes, no);
		
		// see if yes
		if(c == yes) {
			gw.exit();
		}
		
		return;
	}
}
