package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.events.ActionEvent;

public class CommandBrake extends Command {
	
	// game world referring to
	private GameWorld gw;
	
	/**
	 * constructor for brake command
	 * @param gw
	 */
	public CommandBrake(GameWorld gw) {
		
		// pass to parent
		super("Brake");
		
		// set gw
		this.gw = gw;
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		
		// decelerate
		gw.setPlayerSpeed(-1);
	}
}