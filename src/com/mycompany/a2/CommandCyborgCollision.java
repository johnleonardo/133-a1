package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.events.ActionEvent;

public class CommandCyborgCollision extends Command {
	
	// game world were referring to
	private GameWorld gw;
	
	/**
	 * constructor for this command
	 * @param gw
	 */
	public CommandCyborgCollision(GameWorld gw) {
		
		// pass to parent
		super("Collided with NPC");
		
		// set gw
		this.gw = gw;
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		// mark collision with NPC
		gw.collidedWith(0);
	}
}