package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.table.TableLayout;

public class CommandHelp extends Command {

	// game world
    private GameWorld gw;
    
    /**
     * constructor for this command
     * @param gw
     */
    public CommandHelp(GameWorld gw) {
    	
    	// pass to parent
        super("Help");
        
        // set gw
        this.gw = gw;
    }
    
    @Override
    public void actionPerformed(ActionEvent ev) {

    	// create a help box
        Dialog helpBox = new Dialog("Help", new TableLayout(10, 2));

        // display key bindings to user
        helpBox.add(new Label("Command"));
        helpBox.add(new Label("Key Bindings"));
        helpBox.add(new Label("Accelerate"));
        helpBox.add(new Label("a"));
        helpBox.add(new Label("Brake"));
        helpBox.add(new Label("b"));
        helpBox.add(new Label("Left Turn"));
        helpBox.add(new Label("l"));
        helpBox.add(new Label("Right Turn"));
        helpBox.add(new Label("r"));
        helpBox.add(new Label("Collide w/ Energy Station"));
        helpBox.add(new Label("f"));
        helpBox.add(new Label("Collide w/ NPC"));
        helpBox.add(new Label("s"));
        helpBox.add(new Label("Tick"));
        helpBox.add(new Label("t"));
        helpBox.add(new Label("Exit"));
        helpBox.add(new Label("x"));

        // okay command
        Command okCommand = new Command("ok");
        
        // show
        Command c = Dialog.show("", helpBox, okCommand);
        
        // do nothing
        if (c == okCommand) {
            return;
        }

    }

}