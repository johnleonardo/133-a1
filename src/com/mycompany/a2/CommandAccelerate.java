package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.events.ActionEvent;

public class CommandAccelerate extends Command {
	
	
	// game world
	private GameWorld gw;
	
	/**
	 * @param gw
	 */
	public CommandAccelerate(GameWorld gw) {
		// pass to parent
		super("Accelerate");
		
		// set gw
		this.gw = gw;
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		System.out.println("Accelerating...");
		
		// set speed
		gw.setPlayerSpeed(1);
	}

}