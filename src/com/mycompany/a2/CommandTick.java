package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.events.ActionEvent;

public class CommandTick extends Command {
	
	// game world were referring to
    private GameWorld gw;
    
    /**
     * constructor for this command
     * @param gw
     */
    public CommandTick(GameWorld gw) {
    	
    	// pass to parent
        super("Tick");
        
        // set gw
        this.gw = gw;
    }
    
    @Override
    public void actionPerformed(ActionEvent ev) {
    	
    	// tick
        gw.tick();
    }
}