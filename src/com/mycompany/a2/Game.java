package com.mycompany.a2;

// imports
import com.codename1.ui.*;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import java.lang.String; 
import com.codename1.charts.util.ColorUtil;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import java.util.Observable; 

public class Game extends Form {
	
	// game attributes
	private GameWorld gw;
	private ScoreView scoreView;
	private MapView mapView;
	
	
	public Game() {
		
		// set attributes
		gw = new GameWorld();
		scoreView = new ScoreView(gw);
		mapView = new MapView(gw);
		
		// add observers to game world
		gw.addObserver(scoreView);
		gw.addObserver(mapView);
		
		// set layout as border layout
		this.setLayout(new BorderLayout());
		
		// initialize exit command
		Command exitCommand = new CommandExit(gw);
		
		// initialize accelerate command
		Command accelerateCommand = new CommandAccelerate(gw);
		
		// initialize left command
		Command leftCommand = new CommandLeftHeading(gw);
		
		// initialize right command
		Command rightCommand = new CommandRightHeading(gw);
		
		// initialize brake command
		Command brakeCommand = new CommandBrake(gw);
		
		// initialize drone collision command
		Command droneCollisionCommand = new CommandDroneCollision(gw);
		
		// initialize cyborg collision command
		Command cyborgCollisionCommand = new CommandCyborgCollision(gw);
		
		// initialize energy station collision command
		Command energyCommand = new CommandEnergyStationCollision(gw);
		
		// initialize tick command
		Command tickCommand = new CommandTick(gw);
		
		// add key listeners for each of these commands that were just initialized
		addKeyListener('x', exitCommand);
		addKeyListener('a', accelerateCommand);
		addKeyListener('b', brakeCommand);
		addKeyListener('l', leftCommand);
		addKeyListener('r', rightCommand);
		addKeyListener('f', energyCommand);
		addKeyListener('g', droneCollisionCommand);
		addKeyListener('t', tickCommand);
		
		// West Container
		Container westContainer = new Container();
		westContainer.getAllStyles().setBorder(Border.createLineBorder(4, ColorUtil.GRAY));
		westContainer.setLayout(new BoxLayout(2));
		
		// Accelerate
		Button accelerateButton = new Button("Accelerate");
		accelerateButton.setCommand(accelerateCommand);
		westContainer.addComponent(accelerateButton);
		accelerateButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		accelerateButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		accelerateButton.getAllStyles().setBgTransparency(255);
		accelerateButton.getAllStyles().setMarginBottom(10); 
		
		
		
		// Left
		Button leftButton = new Button("Left");
		leftButton.setCommand(leftCommand);
		westContainer.addComponent(leftButton);	
		leftButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		leftButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		leftButton.getAllStyles().setBgTransparency(255);	
		leftButton.getAllStyles().setMarginBottom(10);
		
		// change strategies
		Button changeButton = new Button("Change Strategies");
		westContainer.addComponent(changeButton);	
		changeButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		changeButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		changeButton.getAllStyles().setBgTransparency(255);	
		changeButton.getAllStyles().setMarginBottom(10);
		
		// East Container with brake and right
		Container eastContainer = new Container();
		eastContainer.getAllStyles().setBorder(Border.createLineBorder(4, ColorUtil.GRAY));
		eastContainer.setLayout(new BoxLayout(2));
		
		// Brake
		Button brakeButton = new Button("Brake");
		brakeButton.setCommand(brakeCommand);
		eastContainer.addComponent(brakeButton);
		brakeButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		brakeButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		brakeButton.getAllStyles().setBgTransparency(255);	
		brakeButton.getAllStyles().setMarginBottom(10);
		
		// Right
		Button rightButton = new Button("Right");
		rightButton.setCommand(rightCommand);
		eastContainer.addComponent(rightButton);
		rightButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		rightButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		rightButton.getAllStyles().setBgTransparency(255);	
		rightButton.getAllStyles().setMarginBottom(10);
		
		Container centerContainer = new Container();
		centerContainer.getAllStyles().setBorder(Border.createLineBorder(4,ColorUtil.MAGENTA));
		
		// South Container
		Container southContainer = new Container();
		southContainer.getAllStyles().setBorder(Border.createLineBorder(4, ColorUtil.GRAY));
		southContainer.setLayout(new FlowLayout(Component.CENTER));
		
		// Collide with NPC
		Button NPCButton = new Button("Collide with NPC");
		southContainer.addComponent(NPCButton);
		NPCButton.setCommand(cyborgCollisionCommand);
		NPCButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		NPCButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		NPCButton.getAllStyles().setBgTransparency(255);	
		NPCButton.getAllStyles().setMarginRight(5);
		
		// Collide with base
		Button baseButton = new Button("Collide with Base");
		baseButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		baseButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		baseButton.getAllStyles().setBgTransparency(255);	
		baseButton.getAllStyles().setMarginRight(5);
		southContainer.addComponent(baseButton);
		
		// Collide with Drone
		Button collideButton = new Button("Collide with Drone");
		southContainer.addComponent(collideButton);
		collideButton.setCommand(droneCollisionCommand);
		collideButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		collideButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		collideButton.getAllStyles().setBgTransparency(255);	
		collideButton.getAllStyles().setMarginRight(5);
		
		// Collide with Energy
		Button energyButton = new Button("Collide with Energy Station");
		southContainer.add(energyButton);
		energyButton.setCommand(energyCommand);
		energyButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		energyButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		energyButton.getAllStyles().setBgTransparency(255);	
		energyButton.getAllStyles().setMarginRight(5);
		
		// Tick/game time
		Button tickButton  = new Button("Tick");
		tickButton.setCommand(tickCommand);
		southContainer.add(tickButton);
		tickButton.getAllStyles().setFgColor(ColorUtil.WHITE);
		tickButton.getAllStyles().setBgColor(ColorUtil.BLUE);
		tickButton.getAllStyles().setBgTransparency(255);	
		tickButton.getAllStyles().setMarginRight(5);
		
		// setup the toolbar for the guit 
		Toolbar toolbar = new Toolbar();
		this.setToolbar(toolbar);
		toolbar.setTitle("Robo Track ");
		
		// accelerate, left, and change
		toolbar.addCommandToSideMenu(accelerateCommand);
		Command soundCommand = new CommandSound(gw);
		CheckBox soundCheck = new CheckBox("Sound");
		soundCheck.setCommand(soundCommand);
		toolbar.addComponentToSideMenu(soundCheck );
		Command aboutInfoCommand = new CommandAbout(gw);
		toolbar.addCommandToSideMenu(aboutInfoCommand);
		
		// for quittitng
		toolbar.addCommandToSideMenu(exitCommand);	
		
		// help button
		Command helpButton = new CommandHelp(gw);
		toolbar.addCommandToRightBar(helpButton);
		
		// set corner sections
		this.add(BorderLayout.WEST, westContainer);
		this.add(BorderLayout.EAST, eastContainer);
		this.add(BorderLayout.SOUTH, southContainer);
		this.add(BorderLayout.NORTH, mapView);
		this.add(BorderLayout.CENTER, scoreView);
		
		// set map height and width
		gw.setMapHeight(mapView.getComponentForm().getHeight());
		gw.setMapWidth(mapView.getComponentForm().getWidth());
		
		// initialize and show
		gw.init();
		this.show();



		
	}	
	private void play() {
		
		// command label
		Label myLabel = new Label("Enter a Command:");
		
		// add it
		this.addComponent(myLabel);
		
		// my text field
		final TextField myTextField = new TextField();
		
		// add it
		this.addComponent(myTextField);
		
		// show new UI
		this.show();
		
		// add action listener to text field
		myTextField.addActionListener(new ActionListener(){
		
			// action that will be fired
			public void actionPerformed(ActionEvent evt) {
				
				// get command
				String sCommand = myTextField.getText().toString();
				
				// clear txt field
				myTextField.clear();
				
				// get command
				char c = sCommand.charAt(0);
				
				// switch
				switch (c) {
				case 'x':
					// confirm exit w/ label
					myLabel.setText("Please enter y or n");
					
					// quit game
					gw.quitGame();
					
					break;
				
				case 'a':
					
					// accelerate
					gw.setPlayerSpeed(1);
					break;
				
				case 'b':
					
					// decelerate
					gw.setPlayerSpeed(-1);
					break;
				
				case 'l':
					
					// change heading
					gw.changeHeading(-5);
					break;
				
				case 'r':
					
					// change heading
					gw.changeHeading(5);
					break;
				
				case 'c':
					
					// collided w/ cyborg
					gw.collidedWith(0);
					break;
					
				case 'e':
					
					// collided w/ energy station
					gw.collidedWithEnergyStation();
					break;
				
				case 'g':
					
					// collided with drone
					gw.collidedWith(1);
					break;
				
				case 't':
					
					// tick
					gw.tick();;
					break;
				
				case 'd':
					
					// display
					gw.display();
					break;
				
				case 'm':
					
					// output map
					gw.map();
					break;
				
				case 'n':
					
					// mark as not wanting to quit
					gw.noQuit();
					
					// reset label
					myLabel.setText("Enter a Command:");
					break;
				
				case 'y':
					
					// exit
					gw.exit();
					break;
					
				default:
					// check if digit between 0 and 9
					if (Character.isDigit(c) && Integer.parseInt(String.valueOf(c)) <= 9 && Integer.parseInt(String.valueOf(c)) >= 1 ) {
						// collide with energy station
						gw.collidedWithBase(c - '0');
					}
					break;
				}
			}
		});
	}
	
}

