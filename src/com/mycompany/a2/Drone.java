package com.mycompany.a2;

import java.util.Random;

import com.codename1.charts.util.ColorUtil;

public class Drone extends Movable {

	// random instance
	private Random rand = new Random();
	
	/**
	 * Constructor
	 * @param size
	 * @param x
	 * @param y
	 */
	public Drone(int size, double x, double  y) {
		// call parent's constructor
		super(size, x, y);
		
		// set color
		super.setColor(ColorUtil.rgb(190, 1, 1));
		
		// set random speed
		setSpeed(5 + rand.nextInt(5));
		
		// set random heading
		setHeading(rand.nextInt(359));
	}
	
	@Override
	public String toString() {
		String parentDesc = super.toString();
		return "Drone:" + parentDesc;
	}
	
	// not allowed to change color
	@Override
	public void setColor(int color) {}
	
	// set random heading for drones
	@Override
	public void setHeading(int heading) {
		// set heading w/ random value 
		super.setHeading((-5)+rand.nextInt(10));
	}
}
