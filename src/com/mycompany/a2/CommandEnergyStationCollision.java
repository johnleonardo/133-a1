package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.events.ActionEvent;

public class CommandEnergyStationCollision extends Command {
	
	// game world were referring to
	private GameWorld gw;
	
	/**
	 * constructor for this command
	 * @param gw
	 */
	public CommandEnergyStationCollision(GameWorld gw) {
		
		// pass to parent
		super("Collided with Energy Station");
		
		// set gw
		this.gw = gw;
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		// mark collision
		gw.collidedWithEnergyStation();
	}
	
}
