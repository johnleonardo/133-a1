package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.table.TableLayout;

public class CommandAbout extends Command {
	
	// game world
	private GameWorld gw;

	/**
	 * constructor for this command
	 * @param gw
	 */
	public CommandAbout(GameWorld gw) {
		
		// pass to parent
		super("About");
		
		// set gw
		this.gw = gw;
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		
		// create about box
		Dialog aboutBox = new Dialog("About", new TableLayout(4,1));
		
		// okay command
		Command okCommand = new Command("ok");
		
		// add about components
		aboutBox.add(new Label ("Name: John Leonardo"));
		aboutBox.add(new Label ("CSC 133"));
		aboutBox.add(new Label ("Version: A2"));
		
		// command
		Command c = Dialog.show("",  aboutBox, okCommand);
		
		// do nothing
		if (c == okCommand) {
			return;
		}
		
	}
	

}