package com.mycompany.a2;

// imports
import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;
import com.codename1.charts.models.Point;
import com.codename1.charts.util.ColorUtil;

public class GameWorld extends Observable {
	
	// random instance
	private Random rand = new Random();
	
	// player
	private PlayerCyborg player;
	
	// game info
	private int lives;
	private int counter = 0;
	private int clock = 0;
	
	// sound stuff
	private boolean soundOn = false;
	private boolean soundChecked = false;
	
	// map config
	private int mapHeight;
	private int mapWidth;

	// preconfigured sizes
	private int baseSize = 15;
	private int playerSize = 25;
	
	// flag for exiting
	private boolean isExit = true;
	
	// winning base
	private int winningBase = 4;
	
	// game object collection
	GameObjectCollection collection;
	
	public GameWorld() {
		
		// create collection
		collection = new GameObjectCollection();
		
		// set our player
		this.player = new PlayerCyborg(this.playerSize, 0, 500, 500);
	}
	
	// exit the game
	public void exit() {
		if (isExit) System.exit(0);
	}
	
	public void quitGame() {
		System.out.println("Do you wish to exit? (y/n)");
		
		// set flag
		isExit = true;
	}
	
	public void noQuit() {
		// set flag
		isExit = false;
	}
	
	/**
	 * @param height
	 */
	public void setMapHeight(int height) {
		this.mapHeight = height;
	}
	
	/**
	 * @param width
	 */
	public void setMapWidth(int width) {
		this.mapWidth = width;
	}
	
	/**
	 * this is what initializes and starts the game
	 */
	public void init() {
		
		// add Fixed objects
		collection.add(new Base(baseSize, 1,500, 500));
		collection.add(new Base(baseSize, 2,100, 750));
		collection.add(new Base(baseSize, 3,400, 400));
		collection.add(new Base(baseSize, 4,1000, 125));
		collection.add(new EnergyStation(15 + rand.nextInt(25),rand.nextInt(mapWidth),rand.nextInt(mapHeight)));
		collection.add(new EnergyStation(15 + rand.nextInt(25),rand.nextInt(mapWidth),rand.nextInt(mapHeight)));
		
		// add player
		collection.add(this.player);
		
		// add NPC's
		collection.add(new NonPlayerCyborg(this.playerSize, 0, 500, 550));
		collection.add(new NonPlayerCyborg(this.playerSize, 0, 500, 450));
		collection.add(new NonPlayerCyborg(this.playerSize, 0, 550, 500));
		
		
		// add drones
		collection.add(new Drone(15 + rand.nextInt(25), rand.nextInt(mapWidth), rand.nextInt(mapHeight)));
		collection.add(new Drone(15 + rand.nextInt(25), rand.nextInt(mapWidth), rand.nextInt(mapHeight)));
		
		// notify observers
		this.setChanged();
		this.notifyObservers();
		
		
	}
	
	/**
	 * Displays information about the game
	 */
	public void display() {
		
		// build string
		String myDesc = "Lives left: " + this.player.getLives()
		+	", Current clock time: " + counter + ", Last base reached: " + this.player.getLastBase() + ", Energy Level: "
		+ this.player.getEnergyLevel() + ", Damage: " + this.player.getDamageLevel();
		
		// log it
		System.out.println(myDesc);
		
	}
	
	// map function
	public void map() {
		
		// line break
		System.out.println();
		
		// get iterator
		IIterator elements = collection.getIterator();
		
		// iterate thru elements
		while (elements.hasNext()) {
			
			// make temp
			GameObject temp = ((GameObject) elements.getNext());
			
			// log this object
			System.out.println(temp.toString());
		}
	}
	
	private void resetLives() {
		// reset player
		this.player.reset();
	}
	
	public void toggleSound() {
		// invert sound
		this.soundChecked = !this.soundChecked;
		
		// notify
		this.setChanged();
		this.notifyObservers();
	}
	
	public void tick() {
		// make sure not max damage, low energy, or out of lives
		if (player.getDamageLevel() != 100 && player.getEnergyLevel() != 0 && player.getLives() != 0 ) {
			
			// set new heading
			this.player.setHeading(player.getSteeringDirection());
			
			// set new energy (based on consumption rate
			this.player.setEnergyLevel();
			
			// increment counter for clock
			incrementCounter();
		
			// get iterator
			IIterator elements = collection.getIterator();
			
			// iterate thru elements
			while (elements.hasNext()) {
				
				// get current object
				GameObject cur = ((GameObject) elements.getNext());
				
				// check if movable
				if (cur instanceof Movable) {
					
					// check if drone
					if (cur instanceof Drone) {
						// set heading first
						((Movable) cur).setHeading(((Movable) cur).getHeading());
						
						// move
						((Movable) cur).move();
					} else {
						// just move
						((Movable) cur).move();
					}
				}
			}
		} else {
			
			// check if still has lives
			if(this.player.getLives()!=0) {
				// reset player (and decrease lives)
				resetLives();
			} else {
				// log gameover
				System.out.println("Game Over");
			}
		}
		
		// notify all
		notifyAllObservers();
	}
	
	/**
	 * @return last base reached
	 */
	public int getLastBaseReached() {
		return this.player.getLastBase();
	}
	
	
	/**
	 * @return the energy level of the player
	 */
	public int getEnergyLevel() {
		return this.player.getEnergyLevel();
	}
	

	/**
	 * @return player's damage level
	 */
	public int getDamageLevel() {
		return this.player.getDamageLevel();
	}
	
	
	public String isSound() {
		// string variant of sound checked
		return (this.soundChecked) ? "ON" : "OFF";
	
	}
	
	/**
	 * @param o : 0 for cyborg collision, 1 for drone collision
	 */
	public void collidedWith(int o) {
		// pass collision to player
		this.player.collidedWith(o);
		
		// get iterator
		IIterator iterator = collection.getIterator();
		
		// iterate thru elements
		while (iterator.hasNext()) {
			
			// get current element
			GameObject cur = (GameObject) iterator.getNext();
			
			// check if NPC
			if (cur instanceof NonPlayerCyborg) {
				
				// mark collision with cyborg
				((NonPlayerCyborg) cur).collidedWith(0);
				
				// break loop
				break;
			}
		}
		
		// notify all
		notifyAllObservers();
	}
	
	
	/**
	 * @param baseNumber
	 */
	public void collidedWithBase(int baseNumber) {
		// mark as collided with base
		this.player.collidedWithBase(baseNumber);
		
		// notify all
		notifyAllObservers();
		
	}
	
	/**
	 * @param delta
	 */
	public void setPlayerSpeed(int delta) {
		
		// set speed on player
		this.player.setSpeed(this.player.getSpeed() + delta);
		
		// notify all
		notifyAllObservers();
	}
	
	/**
	 * @param delta
	 */
	public void changeHeading(int delta) {
		
		// pass to player
		this.player.setHeading(delta);
		
		// notify all
		notifyAllObservers();
	}
	
	
	/**
	 * @return player lives
	 */
	public int getLives() {
		return this.player.getLives();
	}
	
	
	/**
	 * @return the clock
	 */
	public int getClock() {
		return counter;
	}
	
	public void collidedWithEnergyStation() {
		
		// get iterator
		IIterator iterator = collection.getIterator();
		
		// iterate thru game objects
		while (iterator.hasNext()) {
			
			// get current element
			GameObject cur = (GameObject) iterator.getNext();
			
			// check if energy station
			if (cur instanceof EnergyStation) {
				
				// make sure capacity isnt lowest
				if (((EnergyStation) cur).getCapacity() != 0) {
					
					// set energy level to capacity
					this.player.setEnergyLevel(((EnergyStation) cur).getCapacity());
					
					// drain station
					((EnergyStation) cur).setCapacity(0);
					
					// set color to green
					cur.setColor(ColorUtil.rgb(0, 255, 0));
					
					// break out of loop
					break;
				}
			}
		}
		
		// add another energy station w/ random size, width, and height
		collection.add(new EnergyStation(15 + rand.nextInt(25), rand.nextInt(mapWidth), rand.nextInt(mapHeight)));
	}
	
	// to increment counter
	public void incrementCounter() {
		counter += 1;
	}
	
	// to mark game over
	public void loss() {
		System.out.println("Game over, you failed!");
		this.player = null;
		
		// exit
		this.exit();
	}
	
	public void win() {
		System.out.println("Game over, you win! Total time: " + this.clock);
	}
	
	// wrapper method to notify all observers
	private void notifyAllObservers() {
		this.setChanged();
		this.notifyObservers();
	}
}
