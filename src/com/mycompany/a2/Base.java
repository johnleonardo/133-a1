package com.mycompany.a2;

// imports
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.models.Point;

public class Base extends Fixed {
	
	// required attributes
	private int sequenceNumber;
	
	
	/**
	 * @param size
	 * @param sequenceNumber
	 * @param x
	 * @param y
	 */
	public Base(int size, int sequenceNumber, double x, double y) {
		// call parent's constructor
		super(size, x, y);
		
		// set color
		super.setColor(ColorUtil.rgb(0, 255, 0));
		
		// set sequence number
		this.sequenceNumber = sequenceNumber;
	}
	
	/**
	 * @return sequence number
	 */
	public int getSequenceNumber() {
		return sequenceNumber;
	}
	
	// not allowed to set color
	@Override
	public void setColor(int color) {
		
	}
	
	// new toString
	@Override
	public String toString() {
		String parentDesc = super.toString();
		String myDesc = " sequence Number=" + sequenceNumber;
		return "Base:" + parentDesc + myDesc;
	}
}
