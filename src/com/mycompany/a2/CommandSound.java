package com.mycompany.a2;

import com.codename1.ui.Command;
import com.codename1.ui.events.ActionEvent;

public class CommandSound extends Command {
	
	// game world
	private GameWorld gw;
	
	/**
	 * constructor for this command
	 * @param gw
	 */
	public CommandSound(GameWorld gw) {
		
		// pass to parent
		super("Sound");
		
		// set gw
		this.gw = gw;
		
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		// toggle sound
		gw.toggleSound();
	}

}