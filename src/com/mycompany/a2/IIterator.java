package com.mycompany.a2;

public interface IIterator {
	
	// methods that classes must implement
	public boolean hasNext();
	public Object getNext();
}
