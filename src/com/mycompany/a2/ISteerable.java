package com.mycompany.a2;

public interface ISteerable {
	// method that allows classes that implement this to turn
	public void turn(int direction);
}
