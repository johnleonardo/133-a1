package com.mycompany.a2;

import com.codename1.charts.util.ColorUtil;

public class NonPlayerCyborg extends Cyborg {

	/**
	 * @param size
	 * @param heading
	 * @param x
	 * @param y
	 */
	public NonPlayerCyborg(int size, int heading, double x, double y) {
		
		// pass to parent's constructor
		super(size, heading, x, y);
	}
	
	/**
	 * @param o : 0 for cyborg collision, 1 for drone collision
	 */
	public void collidedWith(int o) {
		// make sure damage isn't max
		if (super.getDamageLevel() != 100) {
			if (o == 0) {
				// cyborg collision
				super.setDamage(5);
				super.setColor(ColorUtil.rgb((super.getRedShade() - 5), 0, 0));
				super.setRedShade(5);
				super.configureSpeed();
			} else {
				// drone collision
				super.setDamage(1);
				super.setColor(ColorUtil.rgb((super.getRedShade() - 1), 0, 0));
				super.setRedShade(1);
				super.configureSpeed();
			}
		}
	}

}
