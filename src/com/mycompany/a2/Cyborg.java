package com.mycompany.a2;

// imports
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.models.Point;

public class Cyborg extends Movable implements ISteerable {
	
	// required attributes
	private int maximumSpeed = 30;
	private int energyLevel = 100;
	private int energyConsumptionRate = 1;
	private int damageLevel = 0;
	private int lastBaseReached=1;
	private int lives = 3;
	private boolean alive = true;
	private int steeringDirection;
	
	// dynamic red shade (based on damage)
	private int redShade = 255;

	/**
	 * Constructor for robot
	 * @param x
	 * @param y
	 */
	public Cyborg(int size, int heading, double x, double y) {
		// pass to parent's constructor
		super(size, x, y);
		
		// set default speed of 3
		super.setSpeed(3);
		
		// set color to red
		super.setColor(ColorUtil.rgb(redShade, 0, 0));
	}
	
	@Override
	public String toString() {
		// build strings
		String energyLevelDesc = "energy level: " + energyLevel;
		
		String damageLevelDesc = "damage level: " + damageLevel;
		
		String lastBaseDesc = "last base reached: " + lastBaseReached;
				
		// return result string
		return super.toString() + ", " 
								+ energyLevelDesc + ", "
								+ damageLevelDesc + ", "
								+ lastBaseDesc;
	}
	
	/**
	 * @return
	 */
	public int getLives() {
		return lives;
	}
	
	/**
	 * @return
	 */
	public int getRedShade(){
		return this.redShade;
	}
	
	
	/**
	 * @param delta
	 */
	public void setRedShade(int delta) {
		this.redShade -= delta;
	}
	
	/**
	 * @return
	 */
	public int getLastBase() {
		return lastBaseReached;
	}
	
	/**
	 * @return
	 */
	public int getDamageLevel() {
		return damageLevel;
	}
	
	/**
	 * @return
	 */
	public int getEnergyLevel() {
		return energyLevel;
	}
	
	/**
	 * @return
	 */
	public int getSteeringDirection() {
		return steeringDirection;
	}
	
	
	/**
	 * @param damage
	 */
	public void setDamage(int damage) {
		damageLevel += damage;
	}

	/**
	 * @param direction : 0 for left 1 for right
	 */
	@Override
	public void turn(int direction) {
		
		if (direction == 0) {
			// left
			if (steeringDirection >= -35) {
				// decrease by 5 degrees
				this.steeringDirection -= 5;
			}
		} else {
			// right
			if (steeringDirection <= 35) {
				// increase by 5 degrees
				this.steeringDirection += 5;
			}
		}
	}
	
	@Override
	public void setSpeed(int speed) {
		if(speed <= maximumSpeed && speed >= 0) {
			super.setSpeed(speed);
		}
	}
	
	/**
	 * checks and configures the speed for this cyborg
	 */
	protected void configureSpeed() {
		
		// set new max speed
		this.maximumSpeed = (int) (30 - (30 * (damageLevel / 100.00)));
		
		// check if speed over max
		if (getSpeed() > maximumSpeed) {
			// cap at maximum speed
			setSpeed(maximumSpeed);
		}
	}
	
	/**
	 * @param o : 0 for cyborg collision, 1 for drone collision
	 */
	public void collidedWith(int o) {
		// make sure damage isn't max
		if (damageLevel != 100) {
			if (o == 0) {
				// cyborg collision
				damageLevel += 10;
				setColor(ColorUtil.rgb((redShade -= 10), 0, 0));
				configureSpeed();
			} else {
				// drone collision
				damageLevel += 5;
				setColor(ColorUtil.rgb((redShade -= 5), 0, 0));
				configureSpeed();
			}
		}
	}
	
	/**
	 * @param baseNumber
	 */
	public void collidedWithBase(int baseNumber) {
		
		// check if last base
		if (baseNumber == lastBaseReached + 1) {
			// set to last
			lastBaseReached = baseNumber;
		}
	}
	
	/**
	 * @param energy
	 */
	public void setEnergyLevel(int energy) {
		
		// add energy
		energyLevel += energy;
		
		// check if it's over our max. if it is, reset it
		if(energyLevel > 100) energyLevel = 100;
	}
	
	/**
	 * NO PARAMETER : this one is to set energy based on consumption rate
	 */
	public void setEnergyLevel() {
		// set energy based on consumption rate
		energyLevel -= energyConsumptionRate;
	}
	
	
	
	// to reset our player
	public void reset() {
		// set x and y back to default
		setX(500);
		setY(500);
		
		// set config again and decrease lives
		damageLevel = 0;
		energyLevel = 100;
		lives -= 1;
		
	}
	
	
}
